import React from 'react';

const Titles = () => (
  <div className="titles">
    <h1>Weather Finder</h1>
    <p>Whats the temperature, humidity? Lets find out!</p>
  </div>
)

export default Titles;
