import React from 'react';

// const Weather = props => (
const Weather = (props) => (
  <div className="weather">
    { props.city && props.country && <p>{ props.city }, { props.country }</p> }
    { props.temp && <p>Temperature : { props.temp }</p> }
    { props.humidity && <p>Humidity : { props.humidity }</p> }
    { props.desc && <p>Conditions : { props.desc }</p> }
    { props.error && <p>Error : { props.error }</p> }
  </div>
)

export default Weather;
