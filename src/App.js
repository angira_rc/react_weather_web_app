import React, { Component } from 'react'

import Titles from './components/Titles'
import Form from './components/Form'
import Weather from './components/Weather'

import './App.css'

const API_KEY = "3192e01060725b669c0a7019b427f4cd";

class App extends Component {
  state = {
    city: undefined,
    country: undefined,
    description: undefined,
    temperature: undefined,
    humidity: undefined,
    error: undefined
  }

  getWeather = async (e) => {
    e.preventDefault()

    const city = e.target.elements.city.value
    const country = e.target.elements.country.value

    const api_call = await fetch(`http://api.openweathermap.org/data/2.5/weather?q=${city},${country}&appid=${API_KEY}&units=metric`)
    const data = await api_call.json()

    if (city && country) {
      console.log(data)
      this.setState({
        city: data.name,
        country: data.sys.country,
        description: data.weather[0].temp,
        temperature: data.main.temp,
        humidity: data.main.humidity,
        error: ''
      })
    } else {
      this.setState({
        city: undefined,
        country: undefined,
        description: undefined,
        temperature: undefined,
        humidity: undefined,
        error: 'Please enter the values.'
      })
    }
  }

  render () {
    return (
      <div>
        <Titles />
        <div className="container">
          <Form getWeather = { this.getWeather }/>
          <Weather
            city = { this.state.city }
            country = { this.state.country }
            desc = { this.state.description }
            temp = { this.state.temperature }
            humidity = { this.state.humidity }
            err = { this.state.error }
            />
        </div>
      </div>
    )
  }
}

export default App
